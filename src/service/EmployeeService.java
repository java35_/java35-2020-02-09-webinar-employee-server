package service;

import java.io.Serializable;

import api.dto.EmployeeDto;
import db.EmployeeDatabase;

public class EmployeeService {
	EmployeeDatabase db;

	public EmployeeService(EmployeeDatabase db) {
		this.db = db;
	}

	public boolean addEmployee(Serializable requestData) {
		if (requestData == null)
			return false;
		if (!(requestData instanceof EmployeeDto))
			return false;
		EmployeeDto employeeDto = (EmployeeDto)requestData;
		return db.addEmployee(employeeDto);
	}
	
	
}
