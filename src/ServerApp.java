import api.dto.EmployeeController;
import db.EmployeeDatabase;
import service.EmployeeService;
import telran.net.server.ProtocolJava;
import telran.net.server.ServerJava;

public class ServerApp {
	public static void main(String[] args) throws Exception {
		EmployeeDatabase db = new EmployeeDatabase();
		EmployeeService service = new EmployeeService(db);
		
		ProtocolJava protocol = new EmployeeController(service);
		ServerJava server = new ServerJava(protocol, 5000);
		server.run();
	}
}