package db;

import java.util.HashMap;
import java.util.Map;

import api.dto.EmployeeDto;

public class EmployeeDatabase {
	
	Map<Integer, EmployeeDto> employees = new HashMap<>();

	public boolean addEmployee(EmployeeDto employeeDto) {
		if (employeeDto == null)
			return false;
		
		System.out.println("Try to save EmployeeDto: " + employeeDto);
		
		boolean res = employees.putIfAbsent(employeeDto.getId(), employeeDto) == null;
		
		if (res) {
			System.out.println("Saved");
			System.out.println(employees);
		} else {
			System.out.println("CONFLICT - employee already exists");
		}
		
		return res;
	}
	
}
