package api.dto;
import service.EmployeeService;
import telran.net.RequestJava;
import telran.net.ResponseJava;
import telran.net.TcpResponseCode;
import telran.net.server.ProtocolJava;

public class EmployeeController implements ProtocolJava {

	EmployeeService service;
	
	public EmployeeController(EmployeeService service) {
		this.service = service;
	}

	@Override
	public ResponseJava getResponse(RequestJava request) {
		if (request == null)
			return null;
		switch (request.requestType) {
		case "Hire employee":
			boolean res = service.addEmployee(request.requestData);
			if (res)
				return new ResponseJava(TcpResponseCode.OK, res);
			return new ResponseJava(TcpResponseCode.WRONG_REQUEST, res);

		default:
			return new ResponseJava(TcpResponseCode.UNKNOWN, "ERROR");
		}
	}

}
